#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
ino_t get_inode(char*);
void pwd(ino_t);
void name(ino_t,char*,int);
int main()
{
    pwd(get_inode("."));  /
    putchar('\n');
    return 0;
}

void pwd(ino_t this_inode)
{
    ino_t my_inode;
    char its_name[BUFSIZ];
    if (get_inode("..")!=this_inode)                                 
    {
        chdir(".."); 
        name(this_inode,its_name,BUFSIZ);
        my_inode = get_inode(".");
        pwd(my_inode);
        printf("/%s",its_name);
    }
}
void name(ino_t inode,char* namebuf,int buflen)   //找到i-节点对应的文件名，并放在字符数组里
{
    DIR* cdir;
    struct dirent* direntp;
    cdir = opendir(".");

    while((direntp = readdir(cdir)) != NULL)
    {
        if(direntp->d_ino == inode)
        {
            strncpy(namebuf,direntp->d_name,buflen);
            namebuf[buflen-1] = '\0';
            closedir(cdir);
            return;
        }
    }
    fprintf( stderr , "error looking for inode");
    //exit (1) ;
}
ino_t get_inode(char* fname)            //根据文件名，返回-i节点
{
    struct stat info;
    stat( fname, &info);
    return info.st_ino;
}
