#include<stdio.h>
#include<string.h>
#include<stdlib.h>
typedef unsigned float_bits;
int float_f2i(float_bits f)
{
int exp,frac,E;
int  s=f&0x80000000;
exp=(f>>23)&0xff;
frac=(f&0x7fffff)|(1<<23);
E=exp-127;
if(E<0)return 0;
if(E>30) return 0x80000000;
if(E<=23){
frac=frac>>(23-E);
    if(s>>31) return -frac; 
    else return frac;}
else {frac=frac<<(E-23);
if(s>>31) return -frac; 
    else return frac;}
}

int main()
{
int x=0x464dfd33;
int m=0x00000015;
double n=0x7ff80000;
int y,l;
l=float_f2i(x);
printf("%d->%d\n",x,l);
l=float_f2i(m);
printf("%d->%d\n",m,l);
l=float_f2i(n);
printf("%ld->%x\n",n,l);
}
