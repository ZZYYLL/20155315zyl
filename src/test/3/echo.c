#include "csapp.h"
#include <time.h>

void echo(int connfd, char *haddrp)
{
    time_t t;
    struct tm * lt;
    size_t n; 
    char buf[MAXLINE]; 
    rio_t rio;

    Rio_readinitb(&rio, connfd);
    while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) { //line:netp:echo:eof
    //printf("server received %d bytes\n", n);
    printf("\n客户端IP：%s\n",haddrp);
    printf("服务器实现者学号：20155315\n");
    
    time (&t);//获取Unix时间戳
    lt = localtime (&t);//转为时间结构
    printf ("当前时间为：%d/%d/%d %d:%d:%d\n",lt->tm_year+1900, lt->tm_mon+1, lt->tm_mday, lt->tm_hour, lt->tm_min, lt->tm_sec);//输出结果。注意月份取值区间为[0,11]，所以要+1;年份的修改。
    
	
	Rio_writen(connfd, buf, n);
    }
}
