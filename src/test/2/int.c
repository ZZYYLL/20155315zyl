#include<stdio.h>
typedef unsigned char *byte_pointer;
void show_bytes(byte_pointer start,size_t len){
size_t i;
for(i=0;i<len;i++)
printf(" %.2x",start[i]);
printf("\n");
}

void show_int(int x)
{
show_bytes((byte_pointer)&x,sizeof(int));
}

void show_float(float x)
{
show_bytes((byte_pointer)&x,sizeof(float));
}

void show_pointer(void *x)
{
show_bytes((byte_pointer)&x,sizeof(void *));
}

int main()
{
int i = 0x12345678;
char *m;
m=(char *)&i;
if(*m==0x78)
printf("学号20155315的笔记本电脑是小端\n");
else printf("学号20155315的笔记本电脑是大端\n");
}
