#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BACKLOG 1
#define MAXRECVLEN 10240

int main(int argc, char *argv[])
{
    char buf[MAXRECVLEN];
    int listenfd, connectfd;  
    int port;
    struct sockaddr_in server; 
    struct sockaddr_in client; 
    socklen_t addrlen;
    if (argc != 2) {
	fprintf(stderr, "usage: %s <port>\n", argv[0]);
	exit(0);
    }
    port = atoi(argv[1]);
    
    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        
        perror("socket() error. Failed to initiate a socket");
        exit(1);
    }
 
    
    int opt = SO_REUSEADDR;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    bzero(&server, sizeof(server));

    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listenfd, (struct sockaddr *)&server, sizeof(server)) == -1)
    {
        /* handle exception */
        perror("Bind() error.");
        exit(1);
    }
    
    if(listen(listenfd, BACKLOG) == -1)
    {
        perror("listen() error. \n");
        exit(1);
    }

    addrlen = sizeof(client);
    while(1){
        if((connectfd=accept(listenfd,(struct sockaddr *)&client, &addrlen))==-1)
          {
            perror("accept() error. \n");
            exit(1);
          }
        FILE *stream;
        struct timeval tv;
        gettimeofday(&tv, NULL);
          printf("You got a connection from client's ip %s, port %d ",inet_ntoa(client.sin_addr),port);
        
        int iret=-1;
        char d[10240];
         iret = recv(connectfd, buf, MAXRECVLEN, 0);
            if(iret>0)
            {
               strcpy(d,buf);
                stream = fopen(buf,"r");
                bzero(buf, sizeof(buf));
                strcat(buf,"单词数：");
                char s[21];
long int count = 0;
                while(fscanf(stream,"%s",s)!=EOF)
                count++;
                char str[10];
sprintf(str, "%ld", count); 
int n = sizeof(str);
str[n] = '\0'; 
strcat(buf,str);
strcat(buf,"\n");
                
            }else
            {
                close(connectfd);
                break;
            }
            
            send(connectfd, buf, iret, 0); 
    
    close(listenfd); 
    return 0;
}
