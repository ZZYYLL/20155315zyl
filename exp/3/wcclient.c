#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#define MAXDATASIZE 100

int main(int argc, char *argv[])
{
    int sockfd, num;    
    int port;
	char buf[MAXDATASIZE];   
    struct hostent *he;    
    
    if (argc != 4)
    {
        printf("Usage: %s <PORT><IP Address><Filename>\n",argv[0]);
        exit(1);
    }
    port = atoi(argv[1]);
    if((he=gethostbyname(argv[2]))==NULL)
    {
        printf("gethostbyname() error\n");
        exit(1);
    }
    
    if((sockfd=socket(AF_INET,SOCK_STREAM, 0))==-1)
    {
        printf("socket() error\n");
        exit(1);
    }
    bzero(&server,sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr = *((struct in_addr *)he->h_addr);
    if(connect(sockfd, (struct sockaddr *)&server, sizeof(server))==-1)
    {
        printf("connect() error\n");
        exit(1);
    }
char str[MAXDATASIZE] ;
strcpy(str,argv[3]);

if((num=send(sockfd,str,sizeof(str),0))==-1){
      printf("send() error\n");
        exit(1);
    }
    if((num=recv(sockfd,buf,MAXDATASIZE,0))==-1)
    {
        printf("recv() error\n");
        exit(1);
    }
    buf[num-1]='\0';
    printf("%s",buf);

    close(sockfd);
    return 0;
}
